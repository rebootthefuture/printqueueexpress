FROM hypriot/rpi-node
WORKDIR "/"
RUN ["git", "clone", "Ethan8693@bitbucket.org/rebootthefuture/printqueueexpress.git"]
RUN ["npm", "config", "set", "unsafe-perm", "true"]
RUN ["npm", "install", "pm2", "--global"]
RUN ["npm", "config", "set", "unsafe-perm", "false"]
WORKDIR "/printqueueexpress"
RUN ["git", "pull"]
RUN ["npm", "install"]
