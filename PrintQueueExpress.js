"use strict"


  
var express = require('express')
var datastore = require('nedb')
var request = require('request')
var bodyParser = require('body-parser')
var net = require('net')
var cheerio = require('cheerio')
var moment = require('moment');
var jwt = require('jsonwebtoken');
const fs = require("fs");

const exec = require('child_process').exec;

var schedule = require('node-schedule');

const os = require('os');

var rule = new schedule.RecurrenceRule();
rule.minute = [0, 15, 30, 45];
var j = schedule.scheduleJob(rule, checkForNewer);

var killRule = new schedule.RecurrenceRule();
killRule.minute = rule.minute
var jk = schedule.scheduleJob(killRule, killOldJobs);

var compactRule = new schedule.RecurrenceRule();
compactRule.minute = [1, 16, 31, 46]
var jkl = schedule.scheduleJob(compactRule, compactDatabase)

var Express = null
var Database = null

var PrintQueue = {}
var Responses = {}

var secret = 'SSBhbSBhIHByZXR0eSBwcmludCBxdWV1ZQ=='
var version = require(__dirname + '/package').version

var slackURL = 'https://hooks.slack.com/services/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'

init()

function init() {
  console.log('Running version:', version);

  initExpress()
  initDatabase()

  initPrinting()
}

//
// Express
//

function initExpress() {
  if (Express == null) {
    Express = express()

    Express.use(bodyParser.text({ type: '*/*'}))
    //Express.use(jwt({ secret: 'queue' }))
    Express.use(function (req, res, next) {
      if (req.method == 'POST' && req.body != undefined && req.body != {})
      {
        try {
          var decoded = jwt.verify(req.body, secret);
          req.body = decoded.data
          next()
        }
        catch(err) {
          res.status(401).send('invalid token...')
        }
      }
      else {
        next()
      }
    })


    Express.get('/', function(req, res) {
      res.send('Hello World')
    })

    Express.get('/getQueue', function(req, res) {
      res.send(PrintQueue)
    })

    Express.post('/addPrintJob', function (req, res) {
      if (req.body) {
        // Array of jobs
        if (req.body.length != undefined) {
          req.body.map(function(job) {
            Database.insert(job, function(err, doc) {
              if (err) {
                console.log(err)
              }
            })
          })
          res.send('Add Print Job Array')
        }
        // Single Job
        else if (req.body.doctype == "print_job"){
          Database.insert(req.body, function(err, doc) {
            if (err) {
              if (err.errorType == 'uniqueViolated') {
                Database.update({ _id: req.body._id }, req.body, {}, function(err, numUpdated) {
                  if (err) {
                    res.send(err)
                  }
                  else {
                    res.send('Updated Print Job')
                  }
                })
              }
              else {
                res.send(err)
              }
            }
            else {
              res.send('Added Print Job')
            }
          })
        }
        else {
          res.send('Bad JSON')
        }
      }
      else {
        res.send('Bad JSON')
      }
    })

    Express.post('/addPrinter', function (req, res) {
      if (req.body) {
        // Array of Printers
        if (req.body.length != undefined) {
          req.body.map(function(printer) {
            Database.insert(printer, function(err, doc) {
              if (err) {
                if (err.errorType == 'uniqueViolated') {
                  Database.update({ _id: printer._id, $where: function(){ return parseInt(this._rev.split('-')[0]) < parseInt(printer._rev.split('-')[0]) } }, printer, {}, function(error, numUpdated) {

                    if (error) {
                      console.log(error)
                    }
                  })
                }
                else {
                  console.log(err)
                }
              }
            })
          })
          res.send('Printer Array Added')
        }
        // Single Printer
        else if (req.body.doctype == "printer"){
          Database.insert(req.body, function(err, doc) {
            if (err) {
              if (err.errorType == 'uniqueViolated') {
                Database.update({ _id: req.body._id, $where: function(){ return parseInt(this._rev.split('-')[0]) < parseInt(req.body._rev.split('-')[0]) } }, req.body, {}, function(err, numUpdated) {
                  if (err) {
                    res.send(err)
                  }
                  else if (numUpdated == 1) {
                    res.send('Updated Printer')
                  }
                  else {
                    res.send('Up To Date')
                  }
                })
              }
              else {
                res.send(err)
              }
            }
            else {
              res.send('Printer Added')
            }
          })
        }
        else {
          res.send('Bad JSON')
        }
      }
      else {
        res.send('Bad JSON')
      }
    })

    Express.get('/allPrinters', function(req, res) {
      let query = {
        doctype: "printer",
        isActive: true,
        isDeleted: false,
        isBluetooth: false,
        printerOwner: { $exists: false }
      }
      let projection = {
        IPAddress: 1,
        printerName: 1,
        printerModel: 1,
        _id: 1,
      }
      Database.find(query, projection).sort({ printerName: 1 }).exec(function(err, docs) {
        let temp = []
        if (docs) {
          temp = docs.map(function(printer) {
            let status = ''
            let jobCount = 0
            if (PrintQueue[printer.IPAddress]) {
              status = PrintQueue[printer.IPAddress].status
              jobCount = PrintQueue[printer.IPAddress].jobs.length
            }
            printer.status = status
            printer.jobCount = jobCount

            return printer
          })
        }
        res.send(temp)
      })
    })

    Express.post('/allPrinters', function(req, res) {
      let query = {
        doctype: "printer",
        isActive: true,
        isDeleted: false,
        isBluetooth: false,
        printerOwner: { $exists: false }
      }
      let projection = {
        IPAddress: 1,
        printerName: 1,
        printerModel: 1,
        _id: 1,
      }

      if (req.body.doctype == 'device_map'){
        req.body.lastSeen = moment().toISOString()
        req.body.lastIP = req.ip || req.headers['x-forwarded-for'] || req.connection.remoteAddress
        req.body.lastIP = req.body.lastIP.replace('::ffff:', '')
        Database.insert(req.body, function(err, doc) {
          if (err) {
            if (err.errorType == 'uniqueViolated') {
              Database.update({ _id: req.body._id }, req.body, {}, function(err, numUpdated) {
              })
            }
          }
        })
      }

      Database.find(query, projection).sort({ printerName: 1 }).exec(function(err, docs) {
        let temp = []
        if (docs) {
          temp = docs.map(function(printer) {
            let status = ''
            let jobCount = 0
            if (PrintQueue[printer.IPAddress]) {
              status = PrintQueue[printer.IPAddress].status
              jobCount = PrintQueue[printer.IPAddress].jobs.length
            }
            printer.status = status
            printer.jobCount = jobCount

            return printer
          })
        }
        res.send(temp)
      })
    })

    Express.get('/allDevices', function(req, res) {
      let projection = {
        _id: 1,
        deviceUUID: 1,
        deviceInitials: 1,
        deviceName: 1,
        deviceOS: 1,
        softwareVersion: 1,
        lastSeen: 1,
        lastIP: 1,
      }
      Database.find({ doctype: 'device_map' }, projection).sort({ deviceInitials: 1 }).exec(function(err, docs) {
        res.send(docs)
      })
    })

    Express.post('/removeDevice', function (req, res) {
      if (req.body) {
        // Array of device _id
        if (req.body.length != undefined) {
            Database.remove({ _id: { $in: req.body }, doctype: 'device_map' }, { multi: true }, function (err, numRemoved) {
              res.send({ removed: numRemoved })
            })
        }
        else {
          res.send('Bad JSON')
        }
      }
      else {
        res.send('Bad JSON')
      }
    })

    Express.post('/openJobs', function(req, res) {
      let sendPrintString = { printString: 0 }

      // Remove 'printString' from the results unless specified otherwise
      if (req.body.printString) {
        sendPrintString = {}
      }
      // Return open jobs for specified user
      if (req.body.user) {
        Database.find({ doctype: 'print_job', printed: false, updatedBy: req.body.user }, sendPrintString).sort({ createdAt: -1 }).exec(function(err, docs) {
          if (docs) {
            res.send(docs)
          }
          else {
            res.send([])
          }
        })
      }
      // Return all open jobs
      else {
        Database.find({ doctype: 'print_job', printed: false }, sendPrintString).sort({ createdAt: -1 }).exec(function(err, docs) {
          if (docs) {
            res.send(docs)
          }
          else {
            res.send([])
          }
        })
      }
    })

    Express.post('/closedJobs', function(req, res) {
      let sendPrintString = { printString: 0 }

      // Remove 'printString' from the results unless specified otherwise
      if (req.body.printString) {
        sendPrintString = {}
      }
      // Return open jobs for specified user
      if (req.body.user) {
        Database.find({ doctype: 'print_job', printed: true, updatedBy: req.body.user }, sendPrintString).sort({ createdAt: -1 }).exec(function(err, docs) {
          if (docs) {
            res.send(docs)
          }
          else {
            res.send([])
          }
        })
      }
      // Return all open jobs
      else {
        Database.find({ doctype: 'print_job', printed: true }, sendPrintString).sort({ createdAt: -1 }).exec(function(err, docs) {
          if (docs) {
            res.send(docs)
          }
          else {
            res.send([])
          }
        })
      }
    })

    Express.post('/printJob', function(req, res) {
      if (req.body._id) {
        Database.find({ _id: req.body._id, doctype: 'print_job' }, function (err, docs) {
          if (docs && docs.length > 0) {
            res.send(docs[0])
          }
          else {
            res.status(400).send('Job Not Found')
          }
        })
      }
      else {
        res.status(400).send('Invalid JSON')
      }
    })

    Express.post('/jobStatus', function (req, res) {
      if (req.body) {
        // Array of job _id
        if (req.body.length != undefined) {
            Database.find({ _id: { $in: req.body }, doctype: 'print_job' }, function (err, docs) {
              if (docs) {
                let temp = {}
                docs.map(function(doc) {
                  temp[doc._id] = doc.printed
                })
                res.send(temp)
              }
              else {
                res.sendStatus(400)
              }
            })
        }
        else {
          res.send('Bad JSON')
        }
      }
      else {
        res.send('Bad JSON')
      }
    })

    Express.post('/markPrinted', function (req, res) {
      if (req.body) {
        // Array of job _id
        if (req.body.length != undefined) {
            Database.update({ _id: { $in: req.body }, doctype: 'print_job' }, { $set: { printed: true} }, { multi: true }, function (err, updatedCount) {
              res.send({ numRequested: req.body.length ,numUpdated: updatedCount})
            })
        }
        else {
          res.send('Bad JSON')
        }
      }
      else {
        res.send('Bad JSON')
      }
    })

    Express.post('/removeJob', function (req, res) {
      if (req.body) {
        // Array of job _id
        if (req.body.length != undefined) {
            Database.remove({ _id: { $in: req.body }, doctype: 'print_job' }, { multi: true }, function (err, numRemoved) {
              res.send({ removed: numRemoved })
            })
        }
        else {
          res.send('Bad JSON')
        }
      }
      else {
        res.send('Bad JSON')
      }
    })

    Express.get('/oldJobs', function (req, res) {
      let dateStr = moment().format('YYYY-MM-DD')
      Database.find({ doctype: 'print_job', printed: true, createdAt: { $lt: dateStr} }).sort({ createdAt: -1 }).exec(function(err, docs) {
        if (err) {
          res.send(err)
        }
        else {
          res.send(docs)
        }
      })
    })

    Express.get('/killOldJobs', function (req, res) {
      killOldJobs()
      res.send('Killing Old Jobs...')
    })

    Express.post('/restart', function (req, res) {
      if (req.body.restart == true){
        res.send('Restarting Queue...')
        console.log('Restarting queue by GET request...');
        process.exit()
      }
      else {
        res.send('Invalid JSON')
      }
    })

    Express.post('/restartPi', function (req, res) {
      if (req.body.restart == true){
        res.send('Restarting Machine...')
        console.log('Restarting machine by GET request...');

        exec('reboot', function (error, stdout, stderr) {
        })
      }
      else {
        res.send('Invalid JSON')
      }
    })

    Express.post('/deleteAllPrinters', function (req, res) {
      if (req.body.delete) {
        deleteAllPrinters()
      }
    })

    Express.post('/deleteAllDocuments', function (req, res) {
      if (req.body.delete) {
        deleteAllDocuments()
      }
    })

    Express.get('/version', function (req, res) {
      res.send(version)
    })

    Express.listen(9001, function() {
      console.log("Express listening on port 9001...")
    })
  }
}

//
// Database
//

function initDatabase() {
  if (Database == null) {
    Database = new datastore({ filename: 'data.datafile', autoload: true, onload: err => {
      if (err) {
        console.log('There was an error loading the database.');

        let list = os.networkInterfaces()
        let net = null

        if ('zt0' in list) {
          net = list['zt0'][0]
        }

        let fallback = ''
        let title = ''

        if (net != null) {
          fallback = 'PQE at ' + net.address + ' was deleted'
          title = 'Error encountered at ' + net.address + ':'
        }
        else{
          fallback = 'PQE data was deleted'
          title = 'Error encountered at UNKNOWN:'
        }

        
        let fileSize = getFilesizeInMB('data.datafile')
        let addMessage = '\n\nCurrent file size: ' + fileSize + 'MB'
        

        messageSlack(slackURL, {
          fallback: fallback,
          fields: [{
            title: title,
            value: err.message + '\n\n' + err.stack + addMessage + '\n\nDeleting database...'
          }]
        }, function () {
            console.log('Deleting database...');
            exec('cd ' + __dirname + ' ; rm -rf data.datafile', function (error, stdout, stderr) {
              console.log('Restarting...');
              process.exit()
            })
          })
      }
      else {
        console.log('Database loaded successfully');
      }
    }})
  }
}

//
// Printing
//

function initPrinting() {
  updatePrinters()
  updatePrintJobs()
}

function updatePrinters() {
  Database.find({ doctype: "printer", isActive: true, isDeleted: false, isBluetooth: false, printerOwner: { $exists: false } }, function (err, docs) {
    let temp = {}
    docs.map(function(printer) {
      if (PrintQueue[printer.IPAddress]) {
        temp[printer.IPAddress] = PrintQueue[printer.IPAddress]
      }
      else {
        temp[printer.IPAddress] = { status: '', jobs: [], printing: false }
      }

      let model = printer.printerModel

      if (printer.printerModel.toLowerCase().includes('epson')) {
        model = 'epson'
      }
      else if(printer.printerModel.toLowerCase().includes('star')) {
        model = 'star'
      }

      temp[printer.IPAddress].model = model
      temp[printer.IPAddress].name = printer.printerName
    })
    PrintQueue = temp

    updatePrinterStatus()
  })
}

function updatePrinterStatus() {
  for(let printer in PrintQueue) {
    let preStr = ''
    let postStr = ''
    let ip = printer

    let model = PrintQueue[printer].model

    if (model == 'epson') {
      preStr = 'http://epson:epson@'
      postStr = '/istatus.htm'
    }
    else if (model == 'star') {
      preStr = 'http://'
      postStr = '/html/prnstas.htm'
      ip = ip.replace('TCP:', '')
    }

    let url = preStr + ip + postStr

    let options = {
      url: url, 
      timeout: 1000
    }

    if (model == 'epson') {
      options['tunnel'] = true,
      options['strictSSL'] = false
    }

    request(options, function(error, response, body) {

      if (model == 'star') {
        ip = 'TCP:' + ip
      }

      if (error && PrintQueue[printer]) {
        PrintQueue[printer].status = 'Unreachable'
      }
      let status = ''

      if (body)
      {
        let $ = cheerio.load(body)

        if (model == 'epson') {
          let a = $('td[bgcolor=#009900]').text().trim()
          let b = $('td[bgcolor=#ffff00]').text().trim()
          let c = $('td[id=status-error]').text().trim()
          let d = $('td[id=status-normal]').text().trim()

          if (a != '') {
            status = a
          }
          else if (b != '') {
            status = b
          }
          else if (c != '') {
            status = c
          }
          else if (d != '') {
            status = d
          }
        }
        else if (model == 'star') {
          let a = $('pre').text()
          status = $('pre').first().text().replace(/\r?\n|\r/g, '')
          status = status.replace('    ', '\t')
          status = status.replace('    ', '\t')
          status = status.replace('    ', '\t')
          status = status.replace('    ', '\t')
        }
      }

      if (status.split('\t').length > 1) {
        status = status.split('\t')[1]
      }

      if (status == 'Ready') {
        status = 'Online'
      }

      if (PrintQueue[printer]){
        if (status == '') {
          PrintQueue[printer].status = 'Unreachable'
        }
        else {
          PrintQueue[printer].status = status
        }
      }
    })
  }

  setTimeout(updatePrinters, 1000)
}

function updatePrintJobs() {
  Database.find({ doctype: 'print_job', printed: false }).sort({ createdAt: 1 }).exec(function(err, docs) {
    if (docs) {

      for(let printer in PrintQueue) {
        PrintQueue[printer].jobs = []
      }

      docs.map(function(doc) {
        if (PrintQueue[doc.ip]) {
          if (!jobExistsInArray(doc, PrintQueue[doc.ip].jobs)) {
            PrintQueue[doc.ip].jobs.push(doc)
            //PrintQueue[doc.ip].model = doc.printerModel
            //PrintQueue[doc.ip].name = doc.printerName
          }
        }
        else {
          //PrintQueue[doc.ip] = { printing: false, status: "", model: doc.printerModel, name: doc.printerName }
          //PrintQueue[doc.ip].jobs = [doc]
        }
      })
    }
    print()
  })
}

function print() {

  for (let printer in PrintQueue){
    if (PrintQueue[printer].printing == false && PrintQueue[printer].jobs.length > 0 &&
      (PrintQueue[printer].status == 'Online' || PrintQueue[printer].status == 'Ready')) {

      PrintQueue[printer].printing = true

      let job = PrintQueue[printer].jobs[0]
      console.log('Printing ' + job._id + ' to ' + job.ip + '...')

      const client = net.connect({ host: job.ip.replace('TCP:', ''), port: 9100}, function() {
        let buf = Buffer.from(job.printString, 'base64')
        client.write(buf)
        client.end()

      })
      client.on('error', function(error) {
        //console.log(error)
      })
      client.on('close', function(hadError) {
        console.log(job._id + ' to ' +job.ip + ' connection closed, hadError: ', hadError)
        if (PrintQueue[printer]) {
          PrintQueue[printer].printing = false
        }

        if (hadError == false) {
          Database.update({ _id: job._id}, { $set: { printed: true } }, function(err, numReplaced) {
            if (err) {
              console.log(err)
            }

            if (Responses[job._id]) {
              Responses[job._id].send('success')
              delete Responses[job._id]
            }
          })
        }
      })
      client.setTimeout(5000, function() {
        console.log('timed out')
        client.destroy('someError')
      })
    }
  }

  setTimeout(updatePrintJobs, 1000)
}

function jobExistsInArray(job, array) {
  for(var i = 0; i < array.length; i++) {
    if(array[i]._id == job._id) {
      return true
    }
  }
  return false
}

function killOldJobs() {
  console.log('Killing Old Jobs...');
  let dateStr = moment().format('YYYY-MM-DD')
  Database.find({ doctype: 'print_job', printed: true, createdAt: { $lt: dateStr} }).sort({ createdAt: -1 }).exec(function(err, docs) {
    docs.map(function(doc) {
      Database.remove({ _id: doc._id }, function (err, numRemoved) {
      })
    })
  })
}

function deleteAllPrinters() {
  console.log('deleting all printers...');
  Database.remove({ doctype: 'printer' }, { multi: true }, function(err, numRemoved) {
    console.log('Restarting...');
    exit(0)
  })
}

function deleteAllDocuments() {
  console.log('deleting all documents...');
  Database.remove({}, { multi: true }, function(err, numRemoved) {
    console.log('Restarting...');
    exit(0)
  })
}

function compactDatabase() {
  console.log('Compacting Database...');
  Database.persistence.compactDatafile()
}


//
// Update Repository
//

function checkForNewer(){
  console.log('[UPDATER]', 'Calling \'git pull\'...');
  exec('cd ' + __dirname + ' ; git pull', function (error, stdout, stderr) {
    if (error) {
      console.log('[UPDATER]', error);
    }
    if (!error && stdout) {
      console.log('[UPDATER]', stdout);
      if (!stdout.includes('Already up-to-date')) {
        console.log('[UPDATER]', 'Calling \'npm install\'...')

        exec('cd ' + __dirname + ' ; npm install', function (error, stdout, stderr) {
          console.log('[UPDATER]', 'npm complete');
          console.log('[UPDATER]', 'Restarting App...');
          process.exit()
        })
      }
    }
  })
}

//
// Send Message To Slack
//

function messageSlack(webhookURL, body, completed) {

  let options = {
    uri: webhookURL,
    body: JSON.stringify({
      attachments: [body]
    })
  }

  request.post(options, function(error, response, body) {
    if (completed) {
      completed()
    }
  })
}

function getFilesizeInMB(filename) {
    let stats = fs.statSync(filename)
    let fileSizeInBytes = stats.size
    let fileSizeInMegabytes = fileSizeInBytes / 1000000.0
    return fileSizeInMegabytes
}